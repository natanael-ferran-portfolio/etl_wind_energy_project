start_containers:
	cd utils && bash start_containers.sh

configure_cassandra:
	cd utils && bash create_table.sh

open_airflow:
	cp ./utils/ETL_wind_energy_project.py ./utils/dags/ETL_wind_energy_project.py &&\
		xdg-open http://localhost:8080

stop_containers:
	cd utils && docker-compose down --volumes --remove-orphans &&\
		rm -rf ./config ./dags ./logs ./plugins .env
