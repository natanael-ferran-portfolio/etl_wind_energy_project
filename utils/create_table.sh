#!/bin/bash

# log to cassandra's cqlsh to create "wind_energy_project" keyspace
# and "calafate_h2h" table
echo -e "\\e[36mInitiating database configuration...\\e[0m"
docker exec -it utils_cassandra_1 cqlsh -e "\
    CREATE KEYSPACE IF NOT EXISTS wind_energy_project \
        WITH replication = {'class': 'SimpleStrategy', 'replication_factor': 1}; \
    CREATE TABLE IF NOT EXISTS wind_energy_project.calafate_h2h \
        (date TEXT, \
        hour INT, \
        temperature FLOAT, \
        humidity INT, \
        pressure FLOAT, \
        wind_direction INT, \
        wind_speed INT, \
        PRIMARY KEY (date, hour));"

# check if process is successful
if [ $? -eq 0 ]; then
    echo -e "\\e[32;1mConfiguration was made correctly!\\e[0m"
else
    echo -e "\\e[31;1mError: configuration was not successful.\\e[0m"
fi
