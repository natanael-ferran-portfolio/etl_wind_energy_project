import zipfile
import os

# .zip file path
zip_file_path = '/opt/airflow/utils/last_data.zip'

# extraction path
extract_to_directory = '/opt/airflow/utils'

# check if file exists
if os.path.exists(zip_file_path):
    # create directory if not exists
    os.makedirs(extract_to_directory, exist_ok=True)
    # open file
    with zipfile.ZipFile(zip_file_path, 'r') as zip_ref:
        # extract all files from .zip file
        zip_ref.extractall(extract_to_directory)
