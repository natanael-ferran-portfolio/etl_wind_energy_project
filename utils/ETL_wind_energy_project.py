# importing essential libraries
from datetime import datetime, timedelta
from airflow import DAG
from airflow.operators.bash_operator import BashOperator
from airflow.operators.python_operator import PythonOperator

# defining DAG arguments
default_args = {
    'owner': 'nferran',
    'start_date': datetime.today(),
    'email': ['example@mail.com'],
    'email_on_failure': False,
    'email_on_retry': False,
    'retries': 2,
    'retry_delay': timedelta(hours=4), 
}

# defining DAG
dag = DAG(
    'ETL_wind_energy_project',
    default_args=default_args,
    description='ETL for wind energy project in El Calafate',
    schedule_interval='@daily',
)

# defining tasks

# download .zip file from Servicio Meteorológico Nacional webpage
download = BashOperator(
    task_id='download',
    bash_command='curl -o /opt/airflow/utils/last_data.zip \
        https://ssl.smn.gob.ar/dpd/zipopendata.php?dato=datohorario',
    dag=dag,
)

# extract files and remove .zip original file
unzip = BashOperator(
    task_id='unzip',
    bash_command='python /opt/airflow/utils/unzip.py && rm /opt/airflow/utils/last_data.zip',
    dag=dag,
)

# take only relevant data for the project and save into a new file. Remove original file
filter_data = BashOperator(
    task_id='filter_data',
    bash_command='grep "EL CALAFATE AERO" /opt/airflow/utils/datohorario*.txt > \
        /opt/airflow/utils/last_calafate_data.txt && rm /opt/airflow/utils/datohorario*.txt',
    dag=dag,
)

# extract relevant fields and transform to .csv file. Remove original file
transform = BashOperator(
    task_id='transform',
    bash_command='tr -s " " < /opt/airflow/utils/last_calafate_data.txt | cut -d" " -f2-8 | \
        tr " " "," > /opt/airflow/utils/last_calafate_data.csv && \
        rm /opt/airflow/utils/last_calafate_data.txt',
    dag=dag,
)

# here we define a function to convert the date into conventional unix date format 

def unix_date(date):
    # parse the date string to a datetime object
    datetime_object = datetime.strptime(date, "%d%m%Y")

    # format the datetime object into the desired format
    cassandra_date = datetime_object.strftime("%Y-%m-%d")

    return cassandra_date

import pandas as pd

def fix_date():
    # read .csv file
    df= pd.read_csv('/opt/airflow/utils/last_calafate_data.csv', header= None)

    # convert date column to string
    df.iloc[:, 0]= df.iloc[:, 0].astype(str)

    # fix format
    df.iloc[:, 0]= df.iloc[:, 0].apply(lambda x: unix_date(x))

    # export fixed file
    df.to_csv('/opt/airflow/utils/last_calafate_data.csv', index= False, header= False)


transform_date = PythonOperator(
    task_id='transform_date',
    python_callable=fix_date,
    dag=dag,
)

# load data to cassandra's table with fixed data types

from cassandra.cluster import Cluster
import csv
from cassandra.query import BatchStatement

def load():
    # connect to Cassandra
    cluster = Cluster(['cassandra'])
    session = cluster.connect()

    session.set_keyspace('wind_energy_project')

    # prepare CQL query
    insert_query = session.prepare("""
        INSERT INTO calafate_h2h (date, hour, temperature, humidity, pressure, wind_direction, wind_speed)
        VALUES (?, ?, ?, ?, ?, ?, ?)
    """)

    # open the .csv file
    with open('/opt/airflow/utils/last_calafate_data.csv', 'r') as csvfile:
        csvreader = csv.reader(csvfile)
        
        # initialize a BatchStatement object for batch insertion
        batch = BatchStatement()

        for row in csvreader:
            # extract data from each CSV row
            date, hour, temperature, humidity, pressure, wind_direction, wind_speed = row
            
            # converting data to correct types
            hour = int(hour)
            temperature = float(temperature)
            humidity = int(humidity)
            pressure = float(pressure)
            wind_direction = int(wind_direction)
            wind_speed = int(wind_speed)
            
            # add the insertion query to the batch
            batch.add(insert_query, (date, hour, temperature, humidity, pressure, wind_direction, wind_speed))

            # if batch size reaches a certain limit, execute the batch and reset
            if len(batch) >= 4:
                session.execute(batch)
                batch = BatchStatement()

        # execute any remaining batch
        if batch:
            session.execute(batch)

    # close connection
    cluster.shutdown()

load_to_database = PythonOperator(
    task_id='load_to_database',
    python_callable=load,
    dag=dag,
)

# tasks pipeline
download >> unzip >> filter_data >> transform >> transform_date >> load_to_database