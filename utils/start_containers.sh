#!/bin/bash

# this script configures and initiates docker compose
# for further info please visit official docs at
# https://airflow.apache.org/docs/apache-airflow/stable/howto/docker-compose/index.html#fetching-docker-compose-yaml
mkdir -p ./dags ./logs ./plugins ./config
echo -e "AIRFLOW_UID=$(id -u)" > .env
docker-compose build
docker-compose up airflow-init
docker-compose up

