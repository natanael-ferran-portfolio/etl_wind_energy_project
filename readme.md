# ETL Project for Wind Energy Viability Analysis in El Calafate, Santa Cruz, Argentina 

This repository hosts an Extract, Transform, Load (ETL) project designed to gather weather information from El Calafate, Santa Cruz, Argentina, transform it, and load it into a Cassandra database. The main purpose is to conduct a viability analysis for a potential wind farm in the region.

## Project Description

The project consists of the following main components:

1. **Data Extraction:** Weather information is sourced from the [Official Weather Service of Argentina](https://www.smn.gob.ar/). **1** 
2. **Data Transformation:** Downloaded data is processed and prepared for further analysis.
3. **Cassandra Loading:** Transformed data is loaded into a Cassandra database for storage and subsequent analysis.

The entire ETL process is managed and orchestrated using an Apache Airflow workflow (DAG).

## Requirements

Ensure the following components are installed beforehand:

- Docker
- Docker-Compose

## Setup and Execution

To run the application, follow these steps:

1. Clone this repository on your local machine:

```bash
git clone https://gitlab.com/natanael-ferran-portfolio/etl_wind_energy_project.git
```

2. Open the repository in your preferred IDE. 

3. Open a terminal and execute:

```bash
make start_containers
```

This command will automatically download the necessary images and libraries. Please wait for a few minutes. Once `airflow_webserver` is running without issues, proceed to the next step.

4. Open a new terminal and run

```bash
make configure_cassandra
```

This command will automatically create a cassandra keyspace and a cassandra table with the required schema.

5. In the same terminal, please run:

```bash
make open_airflow
```

This will copy the DAG into /dags directory and open the Airflow client in your default browser.

6. Use the following login credentials:

- **User** = airflow
- **Password** = airflow

7. DAG importing will take a few minutes, so please be patient. Once ready, execute the `ETL_wind_energy` DAG.

8. To access the Cassandra CLI, run:

```bash
docker exec -it utils_cassandra_1 cqlsh
```

Now you can perform your queries.

9. To stop running the containers, execute:

```bash
make stop_containers
```

This command will stop all containers and their directories previously created in /utils.

## Project Structure

    utils/ # contains all necessary components to run the application. 
    Makefile # includes simplified commands for easy usage of the application.
    readme.md # provides information about the project.
    LICENSE # contains license information.


## Contributions

Contributions are welcome! If you'd like to improve this project, feel free to submit a pull request.

## License

This project is licensed under the [MIT License](https://mit-license.org/).

## Notes
**1** - 

### Use of Data from the National Meteorological Service (SMN)

This project utilizes data provided by the National Meteorological Service (SMN) in accordance with the [terms and conditions](https://docs.google.com/viewerng/viewer?url=http://www3.smn.gob.ar/dpd/PAD_legales.pdf) set forth by the SMN. Below are some important considerations:

>**Data Source:** The data used in this project were obtained from the National Meteorological Service (SMN) of Argentina.
>
>**Terms and Conditions:** It is important to note that the use of this data is subject to the terms and conditions of use provided by the SMN. It is recommended to carefully review the terms and conditions to ensure proper compliance.
>
>**Purpose of Use:** The SMN data is used in this project for the purpose of demonstrating technical skills. They are not used for commercial purposes or for other purposes not authorized by the SMN.
>
>**Attribution:** SMN data is licensed under the Creative Commons Attribution 2.5 Argentina license. Proper attribution is provided in accordance with the terms of the license.
>
>**Privacy and Security:** Measures are taken to ensure the privacy and security of the data, following best practices and complying with applicable data protection laws and regulations.
>
>It is essential to respect the conditions set forth by the SMN and to use the data in an ethical and legal manner.

---

MIT License

Copyright (c) 2024-present, Natanael Emir Ferrán
